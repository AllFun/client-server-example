package com.edu.clsrex;

import com.edu.clsrex.tasks.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collection;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Slf4j
@SpringBootApplication
public class MainApp {
    public static void main(String[] args) {
        final DefaultApplicationArguments arguments = new DefaultApplicationArguments(args);
        final Optional<Task> task = ofNullable(arguments.getOptionValues("run.task"))
                .map(Collection::stream)
                .map(stringStream -> stringStream.findFirst()
                        .map(Task::from)
                        .orElseThrow(() -> new IllegalArgumentException("Please indicate run.task with the following possible values:" + Task.getPossibleArguments()))
                );

        if (task.isPresent()) {
            task.ifPresent(MainApp::runTask);
        } else {
            runApp();
        }
    }

    private static void runTask(final Task task) {
        log.info("Starting task {}", task);
    }

    private static void runApp() {
        log.info("Starting app...");
    }
}
