package com.edu.clsrex.tasks;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

@ToString
@Getter
@RequiredArgsConstructor
public enum Task {
    GENERATE_CERTS("generate-certs");
    private final String value;

    public static String getPossibleArguments() {
        return '[' + Stream.of(Task.values()).map(Task::getValue).collect(joining(", ")) + ']';
    }

    public static Task from(final String value) {
        return Stream.of(Task.values())
                .filter(task -> task.getValue().equalsIgnoreCase(value))
                .findAny()
                .orElse(null);
    }
}
